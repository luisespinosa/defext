#!/usr/bin/python
# -*- coding: utf-8 -*-
import argparse
from bootstrap import Bootstrapper

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Bootstrap definitions.')
    parser.add_argument('TRAIN', type=str, 
                       help='Path for the training seeds (WCL Dataset)')
    parser.add_argument('DEV', type=str, 
                       help='Path for the development set')
    parser.add_argument('WORKDIR', type=str, 
                       help='Empty folder where bootstrapping occurs')
    requiredNamed = parser.add_argument_group('required named arguments')
    requiredNamed.add_argument('-n', dest='maxiter', required = True,
                       help='Iteration at which bootstrapping ends and the last model is applied to target_corpus')

    """
    parser.add_argument('-t', '--template', type=str, 
                       help='Template used by CRF++')
    parser.add_argument('TARGET', type=str, 
                       help='Document for tagging definitions')
    """
    
    args = parser.parse_args()

    bootstrapper = Bootstrapper(args.TRAIN, args.WORKDIR, check_workdir=False)

    model = bootstrapper.bootstrap(args.DEV, args.maxiter)

    #result = model.annotate(args.TARGET)

