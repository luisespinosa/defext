# README #

This is the version 1.0 of DefExt, a CRF-based bootstrapping approach for extracting definitions from corpora, written in Python. DefExt may be useful for speeding up or semi-automating a glossary or dictionary building procedure, or for Definition Extraction from corpora.

# Summary #

* DefExt takes as input n plain text files in one-sentence-per-line format.
* It runs a dependency parser and extracts a basic lexicographically motivated features.
* Starting from a training set of choice (e.g. definitions from WordNet mixed up with a few distractors), DefExt iteratively bootstraps a target corpus and extracts one definition sentence per iteration, this definition being the sentence in which the classifier has the highest confidence.
* This repository comes with text-level definitional and non-definitional corpora from Navigli and Velardi (2010), in the resources folder.
* A target corpus (from which definitions may be extracted) is also provided. We preprocessed a subset of the ACL-ARC anthology (Bird et al., 2008) and have made it available from https://drive.google.com/drive/folders/0B4dY7B_VR5juenl0YlRrMk43M00?usp=sharing.

# Feature Extraction #

The following command

```
#!bash

python extract_features.py plain_text.txt1 label1 plain_text.txt2 label2
```

* Generates a column-format text file with part-of-speech, dependencies and basic definitional features for each plain_textN document, and labels each sentence as labelN. This process preprocesses and tags input data. 
* Training data must have two classes: **def** and **nodef**.
* The preprocessed file is saved in the same location as input corpora, with the ```_features.data``` suffix.
* If the corpus to be processed is a target corpus (i.e. unknown label), its associated *labeln* tag must be **unk**. For example:
```
python extract_features.py resources/dev_acl-arc_500k_raw.txt unk
```

# Bootstrap #

* Concatenate definitional and non-definitional feature files, e.g. 

```
#!bash
cat resources/wcl_all_def.txt_features.data resources/wcl_all_nodef.txt_features.data > resources/wcl_train.data
```
* Make an empty folder where the temporary files are stored, e.g. */corpus_1*.

```
#!bash
mkdir corpus_1
```

* Start the bootstrapping process, e.g:

```
#!bash
python main.py -n 5 resources/wcl_train.data resources/resources/dev_acl-arc_500k_raw.txt_features.data corpus_1
```

Where -n is the number of iterations to be performed on the corpus, i.e. the number of definitions you aim to extract.

The bootstrapping process will create two folders inside corpus_1: temp and output. Final output will be redirected to the output folder. In my machine, the 5 first definitions extracted from the 500k-sentence acl-arc were the following:

```
tuc is a prototypical natural language processor for english written in prolog . 
the default is the subtask currently in focus in the dialogue . 
a simple spell correction is a part of the system ( essentially 1 character errors ) . 
in order to reduce the number o f s : t : s : t : s : t : s : ( draws acceleration vector in same direction as velocity ) what is the definition of acceleration ? 
it is distributed in a different manner and the & quot ; form - for - form & quot ; translation is not applicable . 
gem ( miller , 1998 ) is a probabilistic semantic grammar that is an outgrowth of the work on the hum system ( miller , 1996 ) , but uses hand - specified knowledge in addition to probability . 
```

Enjoy!

### Dependencies ###

* CRF++ (https://taku910.github.io/crfpp/)
* Spacy (https://spacy.io)

### References ###

Bird, S., Dale, R., Dorr, B. J., Gibson, B. R., Joseph, M. T., Kan, M. Y., ... & Tan, Y. F. (2008, May). The ACL Anthology Reference Corpus: A Reference Dataset for Bibliographic Research in Computational Linguistics. In LREC.

Espinosa-Anke, L., Carlini, R., Ronzano, F. & Saggion, H. (2016). DEFEXT: A Semi Supervised Definition Extraction Tool. In Proceedings of GLOBALEX, Lexicographic Resources for Human Language Technology, co-located with LREC 2016, Portoroz, Slovenia. [PDF](https://arxiv.org/abs/1606.02514).

Navigli, R., & Velardi, P. (2010, July). Learning word-class lattices for definition and hypernym extraction. In Proceedings of the 48th Annual Meeting of the Association for Computational Linguistics (pp. 1318-1327). Association for Computational Linguistics.

### Contact ###

Luis Espinosa-Anke: luis.espinosa@upf.edu