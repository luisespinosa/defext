#!/usr/bin/python
# -*- coding: utf-8 -*-
import codecs
from collections import defaultdict
import os
import shutil
import sys

class SentInfo(object):

	def __init__(self, start, end, label, confidence, avg_probs):

		self.start = start
		self.end = end
		self.label = label
		self.confidence = confidence
		self.avg_probs = avg_probs

	def __repr__(self):

		return "%d %d %s %f" % (self.start, self.end, self.label, self.confidence)

class Bootstrapper(object):

	TEMPLATE = 'resources/template_demo.txt'
	LEARN_TEMPLATE = 'crf_learn -f %(min_freq)d -c %(hyper_param).1f %(template_path)s %(train_path)s %(model_path)s'
	TEST_TEMPLATE = 'crf_test %(vflag)s -m %(model_path)s %(test_path)s > %(results_path)s'
	#SED_TRANSFER_TEMPLATE = "sed -n %(sent_start)d,%(sent_end)dp %(test_path)s >> %(train_path)s"
	SED_TRANSFER_REPLACE_TEMPLATE = "sed -n %(sent_start)d,%(sent_end)dp %(test_path)s | sed 's/\\t%(empty_label)s$/\\t%(token_label)s/g' >> %(train_path)s"
	SED_DELETE_TEMPLATE = 'sed -i %(sent_start)d,%(sent_end)dd %(test_path)s'

	def __init__(self, train_path, workdir, template=TEMPLATE, check_workdir=True):

		self.train_path = train_path
		self.template = template
		self.workdir = workdir
		self.train_lines = 0
		for line in open(train_path):
			self.train_lines += 1
		if check_workdir:
			if not os.path.isdir(self.workdir):
				e = Exception('The work dir does not exist or is not a directory.')
				raise e
			if os.listdir(self.workdir):
				e = Exception('The work dir is not empty.')
				raise e	
		output_dir = os.path.join(self.workdir, 'output')
		if not os.path.exists(output_dir):
			os.makedirs(output_dir)
		self.temp_dir = os.path.join(self.workdir, 'temp')
		if not os.path.exists(self.temp_dir):
			os.makedirs(self.temp_dir)

	def train(self, train_path, model_path):

		cmd_args = {'min_freq': 3,
					'hyper_param': 1.5,
					'template_path': self.template,
					'train_path': train_path,
					'model_path': model_path}
		cmd_str = Bootstrapper.LEARN_TEMPLATE % cmd_args
		print 'Running train command -> ',cmd_str
		os.system(cmd_str) # RUN CRF LEARN

		
	def classify(self, model, test_set, results_path):

		cmd_args = {'vflag': "-v1",
					'model_path': model,
					'test_path': test_set,
					'results_path': results_path
					}
		cmd_str = Bootstrapper.TEST_TEMPLATE % cmd_args
		os.system(cmd_str) # RUN CRF TEST
		print 'Running test command -> ',cmd_str

	def bootstrap(self, dev_path, max_iter):
		
		# cp seed train y original dev to temp
		# cp model from output to temp
		# Get training and testing (dev) datasets
		temp_train = os.path.join(self.temp_dir, 'temp_train.data')
		temp_dev = os.path.join(self.temp_dir, 'temp_dev.data') # I use it in load_dev
		shutil.copy(self.train_path, temp_train)
		os.system('echo "\n" >> ' + temp_train) # Ensure that the end of the train file is \n to avoid bootstrapped sents being glued to original data
		shutil.copy(dev_path, temp_dev)
		os.system('echo "\n" >> ' + temp_dev) # Ensure that the end of the dev file is \n to avoid bootstrapped sents being glued to original data
		# Set variables for temp files for model and classification results
		model_path = os.path.join(self.temp_dir, 'model')
		results_path = os.path.join(self.temp_dir, 'dev_results.data')
		current_iter = 0

		while current_iter < int(max_iter):
			self.train(temp_train, model_path)
			self.classify(model_path, temp_dev, results_path)
			best_def, best_nodef = self.extractNBest(results_path)
			print 'best_def: ',best_def
			print 'best_nodef: ',best_nodef
			if best_def:
				if best_def.start > best_nodef.start:
					self.enrich(temp_train, temp_dev, best_def, 'unk', 'def')
					self.enrich(temp_train, temp_dev, best_nodef, 'unk', 'nodef')
				else:
					self.enrich(temp_train, temp_dev, best_nodef, 'unk', 'nodef')
					self.enrich(temp_train, temp_dev, best_def, 'unk', 'def')
			else:
				break
			current_iter += 1

		out = defaultdict(str)
		bootstrapped = codecs.open(temp_train, 'r').readlines()[self.train_lines:]
		line_idx = 1
		increase = False
		for line in bootstrapped:
			#print 'line -> ',line.split('\t'),' || line_idx:: ',line_idx
			if len(line.split('\t')) > 1:
				label = line.split('\t')[-1].strip()
				if label == 'def':
					out[('def',line_idx)] += line.split('\t')[0]+' '
					increase = True
				else:
					out[('nodef',line_idx)] += line.split('\t')[0]+' '
					increase = True
			elif len(line.split('\t')) <= 1 and increase:
				line_idx += 1
				increase = False

		out = sorted(out.items(), key= lambda x:x[0][1])
		outf = open(self.workdir+'/output/extracted_defs.txt', 'w')
		for elm in out:
			#print elm
			toprint = elm[0][0]+'_'+str(elm[0][1])+'\t'+elm[1]+'\n'.encode('utf-8')
			print toprint
			outf.write(toprint)
		outf.close()
		sys.exit('--Finished processing the target dataset--')
		# Recortar 

	def enrich(self, train_path, test_path, sent_info, empty_label, token_label):

		cmd_args = {'sent_start': sent_info.start,
					'sent_end': sent_info.end,
					'test_path': test_path,
					'empty_label': empty_label,
					'token_label': token_label,
					'train_path': train_path
					}
		#print Bootstrapper.SED_TRANSFER_REPLACE_TEMPLATE % cmd_args, ' <--- Bootstrapper.SED_TRANSFER_REPLACE_TEMPLATE ...'
		cmd_transf_str = Bootstrapper.SED_TRANSFER_REPLACE_TEMPLATE % cmd_args # Same as using self.SED_TRANSFER_REPLACE_TEMPLATE ...
		os.system(cmd_transf_str) # Transfer sentence from dev to train
		cmd_del_str = Bootstrapper.SED_DELETE_TEMPLATE % cmd_args
		os.system(cmd_del_str) # Delete sentence from dev



	def extractNBest(self, classifier_out_path):

		f = open(classifier_out_path, 'r')

		sent_indexes = []
		start = None
		line_count = 0
		sent_probs = []
		tagset = set()
		max_def = None
		max_nodef = None

		for line in f:
			line = line.strip()
			if line.startswith('#') and len(line.split()) == 2: # A token may be '#'
				sent_conf = float(line.replace('#','').strip())
			else:
				if line:			
					last_col = line.split('\t')[-1]
					label, prob_str = last_col.split('/')
					sent_probs.append(float(prob_str))
					tagset.add(label)
					if start is None:
						start = line_count + 1
				else:
					# Empty line				
					if start is not None and len(tagset) == 1:
						avg_probs = sum(sent_probs)/len(sent_probs)
						sent_info = SentInfo(start, line_count+1, label, sent_conf, avg_probs)
						sent_indexes.append(sent_info)
						if sent_info.label == 'def' and (not max_def or sent_conf > max_def.confidence):
							max_def = sent_info
						if sent_info.label == 'nodef' and (not max_nodef or sent_conf > max_nodef.confidence):
							max_nodef = sent_info
						start = None
						tagset = set()
					else:
						# Si la frase tiene mas de una label (DEF y NODEF) la descartamos
						pass
				line_count += 1
		if start <= line_count:
			if len(tagset) == 1:
				avg_probs = sum(sent_probs)/len(sent_probs)
				#sent_info = SentInfo(start, line_count+1, label, sent_conf, avg_probs)
				sent_info = SentInfo(start, line_count, label, sent_conf, avg_probs)
				sent_indexes.append(sent_info)
				if sent_info.label == 'def' and (not max_def or sent_conf > max_def.confidence):
					max_def = sent_info
				if sent_info.label == 'nodef' and (not max_nodef or sent_conf > max_nodef.confidence):
					max_nodef = sent_info
		return max_def, max_nodef

		
		#sed -n 2,3p dev.txt >> train.txt 
		#sed -i 2,3d dev.txt
