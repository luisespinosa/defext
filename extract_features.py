#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
from spacy.en import English
from collections import defaultdict
import codecs

def load_prominence(textfile):
	f = codecs.open(textfile, 'r', 'utf-8')
	out = defaultdict(int)
	for line in f:
		line = line.strip()
		cols = line.split('\t')
		term = cols[0]
		freq = cols[1]
		out[term] = freq
	f.close()
	return out

def generate_dataset(*args):
	args = args[0]
	if len(args) == 2:
		text_file,label =args[0],args[1]
		print 'Processing:: ',text_file,' || With label:: ',label
		extract_features(text_file, label)
	elif len(args) > 2 and len(args) % 2 == 0:
		pairs = zip(args[::2], args[1::2])
		for text_file,label in pairs:
			print 'Processing:: ',text_file,' || With label:: ',label
			extract_features(text_file, label)
	else:
		sys.exit('Please provide an even number of $filename $label pairs as argument(s)')

def extract_features(text_file, label):
	out = []
	f = codecs.open(text_file, 'r', 'utf-8')
	c = 0
	for line in f:
		c += 1
	f.close()
	f = codecs.open(text_file, 'r', 'utf-8')
	cc = 0
	for line in f:
		if cc % 1000 == 0:
			print 'Processed sentence ',cc,' of ',c
			if cc == 50000:
				break
		cc += 1
		line = line.strip().lower()
		sents = nlp(line).sents
		for doc in sents:
			rich_sent = []
			for token in doc:
				rich_token = []
				rich_token.append(token.orth_)
				rich_token.append(token.pos_)
				rich_token.append(token.dep_)
				if token.orth_ in definitional_prom:
					rich_token.append(definitional_prom[token.orth_])
				else:
					rich_token.append("0")
				if token.orth_ in definiens_prom:
					rich_token.append(definiens_prom[token.orth_])
				else:
					rich_token.append("0")
				rich_token.append(label)
				rich_sent.append(rich_token)
			out.append(rich_sent)
	outf = open(text_file+'_features.data', 'w')
	for sent in out:
		for token in sent:
			outf.write('\t'.join([elm.encode('utf-8') for elm in token])+'\n')
		outf.write('\n')
	outf.close()

if __name__ == '__main__':

	args = sys.argv[1:]
	#print 'Args: ',args
	print 'Loading definitional prominence dict'
	definitional_prom = load_prominence('features_data/definitional_prominence_wordfreqs.txt')
	print 'Loading definiens prominence dict'
	definiens_prom = load_prominence('features_data/definiens_prominence_wordfreqs.txt')
	print 'Loading Spacy'
	nlp = English()
	generate_dataset(args)

